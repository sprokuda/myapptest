# encoding: UTF-8

from objectmaphelper import *

o_QQuickWindowQmlImpl = {"type": "QQuickWindowQmlImpl", "unnamed": 1, "visible": True}
decrypt_Button = {"container": o_QQuickWindowQmlImpl, "id": "decButton", "text": "Decrypt", "type": "Button", "unnamed": 1, "visible": True}
textField1_TextField = {"container": o_QQuickWindowQmlImpl, "id": "textField1", "type": "TextField", "unnamed": 1, "visible": True}
encrypt_Button = {"container": o_QQuickWindowQmlImpl, "id": "encButton", "text": "Encrypt", "type": "Button", "unnamed": 1, "visible": True}
inputTextField_TextField = {"container": o_QQuickWindowQmlImpl, "id": "inputTextField", "type": "TextField", "unnamed": 1, "visible": True}
enter_text_to_encrypt_Text = {"container": o_QQuickWindowQmlImpl, "text": "Enter text to encrypt", "type": "Text", "unnamed": 1, "visible": True}
decryptedTextField_TextField = {"container": o_QQuickWindowQmlImpl, "id": "decryptedTextField", "type": "TextField", "unnamed": 1, "visible": True}
publicKeyTextField_TextField = {"container": o_QQuickWindowQmlImpl, "objectName": "publicKeyTextField", "type": "TextField", "visible": True}
encryptedTextField_TextField = {"container": o_QQuickWindowQmlImpl, "id": "encryptedTextField", "type": "TextField", "unnamed": 1, "visible": True}
secretKeyTextField_TextField = {"container": o_QQuickWindowQmlImpl, "id": "secretKeyTextField", "type": "TextField", "unnamed": 1, "visible": True}
