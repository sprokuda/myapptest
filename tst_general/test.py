# -*- coding: utf-8 -*-

import names
import string

def main():
    startApplication("myApp")
    
    test.compare( waitForObjectExists(names.o_QQuickWindowQmlImpl).active,True, "whether window is active")
    
    initialInputText = waitForObjectExists(names.inputTextField_TextField).text
    test.compare(initialInputText.length(),0,"initial state: counting of numbers in initial input text before decrypt button pressed")   
    
    initialDecryptedText = waitForObjectExists(names.decryptedTextField_TextField).text
    test.compare(initialDecryptedText.length(),0,"initial state: counting of numbers in initial decrypted text before decrypt button pressed")   

    mouseClick(waitForObject(names.decrypt_Button))#decrypt button clicked
    
    test.compare( waitForObjectExists(names.o_QQuickWindowQmlImpl).active,True, "whether window is active")
    
    initialInputText = waitForObjectExists(names.inputTextField_TextField).text
    test.compare(initialInputText.length(),0,"initial state: counting of numbers in initial input text before decrypt button pressed")  
    
    initialDecryptedText = waitForObjectExists(names.decryptedTextField_TextField).text
    test.compare(initialDecryptedText.length(),0,"initial state: counting of numbers in initial decrypted text after decrypt button pressed") 
    
    
    mouseClick(waitForObject(names.inputTextField_TextField), 81, 13, Qt.ControlModifier, Qt.LeftButton)#mouse move
    type(waitForObject(names.inputTextField_TextField), "abcdefg")#input some text to encrypt text field
    
    mouseClick(waitForObject(names.encrypt_Button), Qt.ControlModifier, Qt.LeftButton)#encrypt button clicked
    test.compare( waitForObjectExists(names.o_QQuickWindowQmlImpl).active,True, "whether window is active")
    
    mouseClick(waitForObject(names.decrypt_Button), Qt.ControlModifier, Qt.LeftButton)#decrypt button clicked
    test.compare( waitForObjectExists(names.o_QQuickWindowQmlImpl).active,True, "whether window is active")

    msgLength = waitForObjectExists(names.inputTextField_TextField).text.length()
    
    publicKey = waitForObjectExists(names.publicKeyTextField_TextField).text#getting string of public key
    testHex = QString("0x")
    test.compare(publicKey.count(testHex),32,"counting of numbers in public key")
    
    secretKey = waitForObjectExists(names.secretKeyTextField_TextField).text#getting string of public key
    test.compare(secretKey.count(testHex),32,"counting of numbers in secret key")
    
    encryptedText =  waitForObjectExists(names.encryptedTextField_TextField).text#sting to be encrypted
    
    test.compare(encryptedText.count(testHex),16 + msgLength,"counting of numbers in encrypted text")
    
    test.compare(waitForObjectExists(names.inputTextField_TextField).text,
                 waitForObjectExists(names.decryptedTextField_TextField).text,"input to be equal to output")
    
    type(waitForObject(names.inputTextField_TextField), "h")#one more char added
    mouseClick(waitForObject(names.encrypt_Button), Qt.ControlModifier, Qt.LeftButton)#encrypt button clicked
    mouseClick(waitForObject(names.decrypt_Button), Qt.ControlModifier, Qt.LeftButton)#decrypt button clicked
    
    msgLength = waitForObjectExists(names.inputTextField_TextField).text.length()#getting length  of input string
    
    encryptedText =  waitForObjectExists(names.encryptedTextField_TextField).text#sting to be encrypted
    
    test.compare(encryptedText.count(testHex),16 + msgLength,"counting of numbers in encrypted text")
    
    test.compare(waitForObjectExists(names.inputTextField_TextField).text,
                 waitForObjectExists(names.decryptedTextField_TextField).text,"input to be equal to output")
    
    type(waitForObject(names.inputTextField_TextField), "i")#again one more char added
    mouseClick(waitForObject(names.encrypt_Button), Qt.ControlModifier, Qt.LeftButton)#encrypt button clicked
    mouseClick(waitForObject(names.decrypt_Button), Qt.ControlModifier, Qt.LeftButton)#decrypt button clicked
    
    msgLength = waitForObjectExists(names.inputTextField_TextField).text.length()#getting length  of input string
    
    encryptedText =  waitForObjectExists(names.encryptedTextField_TextField).text#sting to be encrypted
    
    test.compare(encryptedText.count(testHex),16 + msgLength,"counting of numbers in encrypted text")
    
    test.compare(waitForObjectExists(names.inputTextField_TextField).text,
                 waitForObjectExists(names.decryptedTextField_TextField).text,"input to be equal to output")
    
    closeWindow(names.o_QQuickWindowQmlImpl)
